FROM nginx:latest
COPY dist/mfe-client/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf