import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SessionComponent } from './components/session/session.component';
import { SessionsClientComponent } from './components/sessions-client/sessions-client.component';

const routes: Routes = [
  { path: 'cinema/:id/sessions', component: SessionsClientComponent },
  { path: 'session/:id', component: SessionComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
