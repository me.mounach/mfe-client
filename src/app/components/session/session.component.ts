import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Session } from '../../models/session/session';
import { Ticket } from '../../models/ticket/ticket';
import { QrcodeService } from '../../services/qrcode.service';
import { SessionService } from '../../services/session.service';
import { TicketService } from '../../services/ticket.service';

declare let paypal: any;

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit {

  session_id: any;
  session: Session = new Session();
  ticket: Ticket = new Ticket();
  tickets: Array<Ticket> = new Array();

  public imagePath: any;
  imgURL: any;

  showPayBtn: boolean = false;

  addScript: boolean = false;
  paypalLoad: boolean = true;
  paypalActions: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private sessionService: SessionService,
    private ticketService: TicketService,
    private qrcodeService: QrcodeService,
    config: NgbModalConfig, 
    private modalService: NgbModal
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
   }

  ngOnInit() {
    this.session_id = this.route.snapshot.paramMap.get("id");
    //console.log(this.session_id);

    this.getSessionInfos();
    this.getSessionTickets();
  }

  //-------------------------
  counter(i: number) {
    return new Array(i);
  }

  //-----------------------
  checkTickets(i: Number | undefined){
    if(this.tickets.filter(ticket => ticket.seat_num == i).length == 1){
      return true;
    }
    else
      return false;
  }

  /* ----------------------- */
  getSessionInfos() {

    let promise = new Promise((resolve, reject) => {
      this.sessionService.getOneSession(this.session_id)
     .subscribe((response: any) => {
       console.log(response);
       this.session = response.body.session;
       resolve(response);
       } ,
     err => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }

  /* ----------------------- */
  getSessionTickets() {

    let promise = new Promise((resolve, reject) => {
      this.sessionService.getSessionTickets(this.session_id)
     .subscribe((response: any) => {
       //console.log(response);
       this.tickets = response.body.tickets;
       resolve(response);
       } ,
     err => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }

  /* ----------------- */
  createTicket() {

    this.createTicketService().then((res: any) => {
      //this.modalService.dismissAll();
      this.showQrcode(res.body.createdTicket._id);
      this.getSessionTickets();
    });
    
  }
  

  /* ----------------------- */
  createTicketService() {
    let promise = new Promise((resolve, reject) => {
      this.ticketService.createTicket(this.ticket)
     .subscribe((response: any) => {
       //console.log(response);
       resolve(response);
       } ,
       (err: any) => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }

  //--------------------------------
  openCreateModal(content: any, i: Number | undefined) {
    this.ticket = new Ticket();
    this.imgURL = false;
    this.showPayBtn = true;
    this.addScript = false;
    this.afterModalOpen();
    this.ticket.seat_num = i;
    this.ticket.session = this.session;
    this.modalService.open(content, { size: 'lg', scrollable: true });
  }

  /* ----------------------- */
  createQrcode(t: any) {
    let promise = new Promise((resolve, reject) => {
      let tmpTicket = {
        ticket: t
      };
      this.qrcodeService.createQrcode(tmpTicket)
     .subscribe((response: any) => {
       resolve(response);
       } ,
       (err: any) => {
       //console.log(  err.status );
       reject(err);
      });
    });

    return promise;
  }

  //---------------------
  showQrcode(t: any) {

    this.createQrcode(t).then((res: any)=> {

      this.imgURL = res.body; 

      /* var reader = new FileReader();      
      reader.readAsDataURL(res.body); 
      reader.onload = (_event) => { 
        this.imgURL = reader.result; 
      } */

    });

  }

  go_back() {
    this.router.navigate(['/cinema/'+this.session?.theater?.cinema?._id+'/sessions']);
  }

  //-----------------paypal----------------------

  /* ------- */
  addPaypalScript() {
    this.addScript = true;
    return new Promise((resolve, reject) => {
      let scripttagElement = document.createElement('script');    
      scripttagElement.src = 'https://www.paypal.com/sdk/js?client-id=ASK3BrtqFsn8khmF5CcJnSDEXdUbrlABe65RgmmkCvrsyuGBRWCIWGwSKz21aob4XgjxgJ0Z6eLlGZOy&currency=EUR';
      scripttagElement.onload = resolve;
      document.body.appendChild(scripttagElement);
    })
  }

  /* --------- */
  afterModalOpen() {
    if (!this.addScript) {
      this.addPaypalScript().then(() => {
        
        paypal.Buttons({
          onInit: (data: any, actions: any) => {
            
            actions.enable();

                this.paypalActions = actions;

          },
          onClick: (e: any) => {

            this.paypalActions.enable();

          },
          createOrder: (data: any, actions: any) => {
            return actions.order.create({
              purchase_units: [{
                  amount: { 
                    value: this.session.price, // amount !!!!!
                    currency: 'EUR',
                    breakdown: {
                      item_total: {
                        currency_code: 'EUR',
                        value: this.session.price // amount !!!!!!
                      }
                    }
                  },
                  description: this.session.original_title,
                  items: [
                    {
                      name: this.session.title + " ("+ this.session.startDate +") ",
                      quantity : 1,
                      unit_amount: {
                        currency_code : "EUR",
                        value: this.session.price // amount !!!!!
                      }
                    }
                  ]
              }]
            });
          },
          onApprove: (data: any, actions: any) => {

            return actions.order.capture().then((details: any) => {
              //Do something when payment is successful.
     
              this.createTicket();
              this.showPayBtn = false;

            })
          },
          style: {
            layout: 'horizontal',
            size: 'responsive',
            color: 'blue',
            shape: 'rect',
            label: 'checkout',
            tagline: 'false'
           }
        }).render('#paypal-button-container');

        this.paypalLoad = false;
      })
    }
  }


  //-----------
  getStrChars_60 (str: String) {
    return str.substr(0, 160);
  }

  getNumPlaces(numP: any) {
    return numP;
  }

}
